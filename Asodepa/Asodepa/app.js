﻿
/**
 * Module dependencies.
 */
var Sequelize = require('sequelize');
var modelo = require('./models/ModelUser');
var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');
var controller = require('./controllers/homeController');


var app = express();




// all environments

app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

app.get('/crearusuario', function (req, res) {
    modelo.CrearUsuario(req, res);
});
//app.get('/prueba', controller.home);

app.get('/registro', routes.registro); //registro

app.get('/', routes.index); //principal

app.post('/valid', function (req, res){ 

    if (req.body.usuario === 'alfredo' & req.body.password === '123')
        res.render('home', { usuario: req.body.usuario });
    else
        res.render('index', { msg: "Usuario o contraseña incorrectas" });

});

app.get('/users', user.list);

http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
    console.log(process.env.PORT);
});
